package comptoirs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import comptoirs.dao.CategorieRepository;
import comptoirs.entity.Categorie;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Edition des catégories, sans gestion des erreurs
 */
@Controller 
@RequestMapping(path = "/comptoirs/categorie") 
public class CategorieController {

	private final CategorieRepository dao;
	
	@Autowired // dao est injecté par Spring
	public CategorieController(CategorieRepository dao) {
		this.dao = dao;
	}

	/**
	 * Affiche toutes les catégories dans la base
	 * @param model pour transmettre les informations à la vue
	 * @return le nom de la vue à afficher ('showCategories.html')
	 */
	@GetMapping(path = "show")
	public	String afficheToutesLesCategories(Model model) {
		model.addAttribute("categories", dao.findAll());
		return "showCategories";
	}	
		
	/**
	 * Montre le formulaire permettant d'ajouter une catégorie
	 * @param categorie initialisé par Spring, valeurs par défaut à afficher dans le formulaire 
	 * @return le nom de la vue à afficher ('formulaireCategorie.html')
	 */
	@GetMapping(path = "add")
	public String montreLeFormulairePourAjout(Categorie categorie) {
		return "formulaireCategorie";
	}
	
	/**
	 * Appelé par le lien 'Modifier' dans 'showCategories.html'
	 * Montre le formulaire permettant de modifier une catégorie
	 * @param categorie à partir du code de la catégorie transmis en paramètre, 
	 *                  Spring fera une requête SQL SELECT pour chercher la catégorié dans la base
	 * @param model pour transmettre les informations à la vue
	 * @return le nom de la vue à afficher ('formulaireCategorie.html')
	 */
	@GetMapping(path = "edit")
	public String montreLeFormulairePourEdition(@RequestParam("code") Categorie categorie, Model model) {
		model.addAttribute("categorie", categorie);
		return "formulaireCategorie";
	}
        
	/**
	 * Appelé par 'formulaireCategorie.html', méthode POST
	 * @param categorie Une catégorie initialisée avec les valeurs saisies dans le formulaire
	 * @return une redirection vers l'affichage de la liste des catégories
	 */
	@PostMapping(path = "save")
	public String ajouteLaCategoriePuisMontreLaListe(Categorie categorie) {
		// cf. https://www.baeldung.com/spring-data-crud-repository-save
		dao.save(categorie); // Ici on peut avoir une erreur (doublon dans un libellé par exemple)
		return "redirect:show"; // POST-Redirect-GET : on se redirige vers l'affichage de la liste		
	}	

	/**
	 * Appelé par le lien 'Supprimer' dans 'showCategories.html'
	 * @param categorie à partir du code de la catégorie transmis en paramètre, 
	 *                  Spring fera une requête SQL SELECT pour chercher la catégorie dans la base
	 * @return une redirection vers l'affichage de la liste des catégories
	 */
	@GetMapping(path = "delete")
	public String supprimeUneCategoriePuisMontreLaListe(@RequestParam("code") Categorie categorie) {
		dao.delete(categorie); // Ici on peut avoir une erreur (S'il y a des produits dans cette catégorie par exemple)
		return "redirect:show"; // on se redirige vers l'affichage de la liste
	}
}
