package comptoirs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import comptoirs.entity.Categorie;
import comptoirs.dao.CategorieRepository;

@Controller 
@RequestMapping(path = "/comptoirs/categorie/produits") 
public class CategorieProduitController {
	@Autowired
	private CategorieRepository dao;
	
	/**
	 * Affiche la vue "Produits pour une catégorie"
	 * @param categorie Le code de la catégorie à afficher (optionnel)
	 * @param model pour transmettre les informations à la vue
	 * @return le nom de la vue à afficher
	 */
	@GetMapping
	// Spring va chercher la catégorie à partue du code passé en paramètre (optionnel)
	// Si pas trouvé, ou si on n'a pas le code, 'categorie' sera null
	public String produitsParCategorie( @RequestParam(name = "code", required = false) Categorie categorie, Model model ) {
		List<Categorie> touteslesCategories = dao.findAll();
		if (categorie == null) // Si on n'a pas trouvé la catégorie demandée
			// On prend la première de la liste
			// TODO : attention, ce code suppose qu'il y a au moins une catégorie dans la base
			categorie = touteslesCategories.get(0);

		// On transmet les informations à la vue
		model.addAttribute("categories", touteslesCategories);
		model.addAttribute("selected", categorie);
		return "categorieProduits";
	}
}
