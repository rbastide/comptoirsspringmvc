package comptoirs.dto;

import java.time.LocalDate;

public interface CommandeProjection {
    Integer getNumero();
    LocalDate getSaisiele();

    interface ClientProjection {
        String getSociete();
        String getContact();
    }

    ClientProjection getClient();
}
